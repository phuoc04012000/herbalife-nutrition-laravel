import React from "react";
import "../css/footer/main.css";

const MainFooter = () => {
  return (
    <footer>
      <div>
        <div className="footer-text">
          <div>
            <img
              src="https://assets.herbalifenutrition.com/content/dam/regional/emea/en_gb/herbalife-nutrition/logos/herbalife-nut-green-small.png"
              alt=""
            />
          </div>
          <div className="copyright-text">
            <p>
              © Herbalife International of America, Inc. CÔNG TY TNHH MTV
              HERBALIFE VIỆT NAM
            </p>
            <p>
              Trụ sở chính: 26 Đường Trần Cao Vân, Phường Võ Thị Sáu, Quận 3,
              Tp.Hồ Chí Minh
            </p>
            <p>Điện thoại: +84-28-38279191</p>
            <p>
              Giấy Chứng nhận Đăng ký Doanh nghiệp số 0309069208, cấp bởi Sở Kế
              Hoạch và Đầu Tư TP.HCM.
            </p>
            <p>
              Giấy CN Đăng ký Hoạt động Bán hàng đa cấp số 020/QLCT-GCN, cấp bởi
              Cục Quản lý Cạnh tranh (nay là Cục Cạnh tranh và Bảo vệ Người tiêu
              dùng) - Bộ Công Thương.
            </p>
          </div>
          <div>
            <img
              src="https://assets.herbalifenutrition.com/content/dam/regional/apac/vi_vn/sites/herbalife_nutrition/web_graphic/logos/2021/11-Nov/footer.png"
              alt=""
            />
          </div>
        </div>

        <div className="footer-links">
          {/* <div class="social-content">
            <ul>
              <li>
                <a
                  href="https://www.facebook.com/HerbalifeVietnam"
                  target="_blank"
                >
                  <i class="icon-facebook-fl-1"></i>
                </a>
              </li>

              <li>
                <a
                  href="https://www.instagram.com/herbalifevietnam/"
                  target="_blank"
                >
                  <i class="icon-instagram-ln-1"></i>
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/channel/UCMAj3usb5riJ50DXVkVR_Pg/featured"
                  target="_blank"
                >
                  <i class="icon-youtube-fl-2"></i>
                </a>
              </li>
            </ul>
          </div> */}
          <div className="policy">
            <nav>
              <ul>
                <h5>Thông tin</h5>
                <li>
                  <a href="https://www.herbalife-vietnam.com/footer-pages/conditions-of-use/">
                    Điều Kiện Sử Dụng
                  </a>
                </li>

                <li>
                  <a href="https://www.herbalife-vietnam.com/footer-pages/conditions-of-use/">
                    Chính sách Bảo Mật
                  </a>
                </li>

                <li>
                  <a href="https://www.herbalife-vietnam.com/footer-pages/conditions-of-use/">
                    Liên Hệ Chúng Tôi
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default MainFooter;
