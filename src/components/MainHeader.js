// eslint-disable-next-line
import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import * as Role from "../constants/role.js";
import {
  isAuthSelector,
  roleIdSelector,
  emailSelector,
  getUserInfo,
} from "../app/reducers/auth/authSlice";
import "../css/navigation/main.css";

const MainHeader = () => {
  // State
  const isAuth = useSelector(isAuthSelector);
  const email = useSelector(emailSelector);
  const roleId = useSelector(roleIdSelector);

  // Event
  const dispatch = useDispatch();

  // Effect hook
  useEffect(() => {
    dispatch(getUserInfo());
  }, [dispatch]);

  return (
    <Fragment>
      <nav className="navigation">
        <div className="brand">
          <div>
            <img
              src="https://assets.herbalifenutrition.com/content/dam/regional/emea/en_gb/herbalife-nutrition/logos/logo.png"
              alt="Logo"
            />
          </div>

          <div className="auth">
            {isAuth ? (
              email
            ) : (
              <Link className="login-link" to="/login">
                Member Login
              </Link>
            )}
          </div>
        </div>

        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>

          <li>
            <Link to="/healthy">Live healthy</Link>
          </li>
          <li>
            <Link to="/about">About Herbalife</Link>
          </li>
        </ul>
      </nav>
    </Fragment>
  );
};

export default MainHeader;
