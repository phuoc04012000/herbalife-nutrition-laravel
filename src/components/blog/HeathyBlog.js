import React from "react";
import "../../css/blog/main.css";
const HeathyBlog = () => {
  return (
    <div className="heathy-category">
      <img
        src="https://images.unsplash.com/photo-1445384763658-0400939829cd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
        alt=""
      />
      <p>Food</p>
    </div>
  );
};

export default HeathyBlog;
