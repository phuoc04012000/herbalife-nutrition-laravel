import React from "react";
import style from "../css/common/main.module.css";
import MainHeader from "../components/MainHeader";
import CourseraStoreImage from "../components/homepage/CourseraStoreImage";
import MainFooter from "../components/MainFooter";

const Home = () => {
  const titleStyle = {
    textAlign: "center",
    color: "#7ac143",
  };

  return (
    <section>
      <MainHeader />
      <div className={style.container}>
        <h1 style={titleStyle}>Herbalife change your life</h1>
        <div>
          <CourseraStoreImage />
        </div>
      </div>
      <MainFooter />
    </section>
  );
};

export default Home;
