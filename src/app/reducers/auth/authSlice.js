import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import Api from "../../../apis/Api";

// Reducer thunk
export const handleLogin = createAsyncThunk("auth/login", async (formData) => {
  const response = await Api().post("/login", formData);
  return response.data;
});

export const getUserInfo = createAsyncThunk(
  "auth/userInfoFetched",
  async () => {
    const response = await Api().get("/users/detail");
    return response.data;
  }
);
const authSlice = createSlice({
  name: "auth",
  initialState: {
    email: "",
    roleId: "",
    isAuth: false,
  },
  reducers: {},

  extraReducers: {
    // Login
    [handleLogin.fulfilled]: (state, action) => {
      const data = action.payload;
      console.log(data);
      localStorage.setItem("token", data.token);
      state.isAuth = true;
    },
    [handleLogin.rejected]: (state, action) => {
      alert("Email or password is not correct, please try again!!!");
    },

    // get user information on
    [getUserInfo.fulfilled]: (state, action) => {
      const data = action.payload;
      state.isAuth = true;
      state.email = data.email;
      state.roleId = data.role_id;
    },
  },
});

// Reducer
export const authReducer = authSlice.reducer;

// Selector
export const emailSelector = (state) => state.authReducer.email;
export const isAuthSelector = (state) => state.authReducer.isAuth;
export const roleIdSelector = (state) => state.authReducer.roleId;

// Action export
